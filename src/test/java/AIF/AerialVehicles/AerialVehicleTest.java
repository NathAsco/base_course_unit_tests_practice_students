package AIF.AerialVehicles;

import AIF.AIFUtil;
import AIF.AerialVehicles.FighterJets.F15;
import AIF.Missions.Mission;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.*;

@RunWith(Parameterized.class)
public class AerialVehicleTest {

    public static AIFUtil aifUtil;
    private final int hoursSinceLastRepair;
    private final int expected;

    public AerialVehicleTest(int hoursSinceLastRepair, int expected) {
        this.hoursSinceLastRepair = hoursSinceLastRepair;
        this.expected = expected;
    }

    @BeforeClass
    public static void setUpBeforeClass() {
        aifUtil = new AIFUtil();
    }

    //ערכי גבול
    @Parameterized.Parameters(name = "{index} input:{0}, output:{1}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                //Limit values
                {-1, 0}, {0, 0}, {1, 1}
        });
    }

    @Test
    public void constructorTest()
    {
        F15 f15 = new F15(anyString(),anyInt(),anyString(),anyString(), any(Mission.class), hoursSinceLastRepair, anyBoolean());
        assertEquals(expected, f15.getHoursOfFlightSinceLastRepair());
    }

    @Test
    public void setHoursOfFlightSinceLastRepairTest()
    {
        F15 f15 = new F15(anyString(),anyInt(),anyString(),anyString(), any(Mission.class), anyInt(), anyBoolean());
        f15.setHoursOfFlightSinceLastRepair(hoursSinceLastRepair);
        assertEquals(expected, f15.getHoursOfFlightSinceLastRepair());
    }
}
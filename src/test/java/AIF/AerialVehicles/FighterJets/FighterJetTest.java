package AIF.AerialVehicles.FighterJets;

import AIF.AIFUtil;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class FighterJetTest {

    public static AIFUtil aifUtil;
    private final int hours;
    private final int expected;

    public FighterJetTest(int hours, int expected) {
        this.hours = hours;
        this.expected = expected;
    }

    @BeforeClass
    public static void setUpBeforeClass() {
        aifUtil = new AIFUtil();
    }

    //ערכי גבול
    @Parameterized.Parameters(name = "{index} input:{0}, output:{1}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                //Limit values
                {-1, 0}, {0, 0}, {1, 1}, {249, 249}, {250, 0}, {251, 0},
                //Middle values
                {-28, 0}, {23, 23}, {299, 0}
        });
    }

    @Test
    public void testCheckFighterLimitAndMiddleValues() {
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("F15"), hours);
        assertEquals("failure - hoursOfFlightSinceLastRepair should be " + expected,
                expected, aifUtil.getAerialVehiclesHashMap().get("F15").getHoursOfFlightSinceLastRepair());
    }
}
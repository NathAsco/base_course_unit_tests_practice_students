package AIF.AerialVehicles.FighterJets;

import AIF.AIFUtil;
import AIF.Missions.MissionTypeException;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class F16Test {

    public static AIFUtil aifUtil;

    @BeforeClass
    public static void setUpBeforeClass(){
        aifUtil = new AIFUtil();
    }

    @Test
    public void testF16AttackMission() {

        try{
            aifUtil.getAerialVehiclesHashMap().get("F16").setMission(aifUtil.getAllMissions().get("attack"));
            assertTrue(true);
        }catch (MissionTypeException missionTypeException){//if exception is thrown it is mean that mission cannot be deploy with the curen aerial vehicle.
            fail();
        }
    }

    @Test//Executable Mission -> AttackMission
    public void testF16BDAMission() {

        try{
            aifUtil.getAerialVehiclesHashMap().get("F16").setMission(aifUtil.getAllMissions().get("bda"));
            assertTrue(true);
        }catch (MissionTypeException missionTypeException){//if exception is thrown it is mean that mission cannot be deploy with the curen aerial vehicle.
            fail();
        }
    }

    @Test(expected = MissionTypeException.class)//Unexecutable Mission -> IntelligenceMission
    public void testF16UnexecutableMission() throws MissionTypeException{
        aifUtil.getAerialVehiclesHashMap().get("F16").setMission(aifUtil.getAllMissions().get("intelligence"));
    }
}
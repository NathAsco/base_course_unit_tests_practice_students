package AIF.AerialVehicles.FighterJets;

import AIF.AIFUtil;
import AIF.Missions.MissionTypeException;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class F15Test {

    public static AIFUtil aifUtil;

    @BeforeClass
    public static void setUpBeforeClass(){
        aifUtil = new AIFUtil();
    }

    @Test
    public void testF15AttackMission() {
        try{
            aifUtil.getAerialVehiclesHashMap().get("F15").setMission(aifUtil.getAllMissions().get("attack"));
            assertTrue(true);
        }catch (MissionTypeException missionTypeException){//if exception is thrown it is mean that mission cannot be deploy with the curen aerial vehicle.
            fail();
        }

    }

    @Test
    public void testF15IntelligenceMission() {
        try{
            aifUtil.getAerialVehiclesHashMap().get("F15").setMission(aifUtil.getAllMissions().get("intelligence"));
            assertTrue(true);
        }catch (MissionTypeException missionTypeException){//if exception is thrown it is mean that mission cannot be deploy with the curen aerial vehicle.
            fail();
        }
    }

    @Test(expected = MissionTypeException.class)//Unexecutable Mission -> BDAMission
    public void testF15UnexecutableMission() throws MissionTypeException{
        aifUtil.getAerialVehiclesHashMap().get("F15").setMission(aifUtil.getAllMissions().get("bda"));
    }
}
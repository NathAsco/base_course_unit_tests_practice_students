package AIF.AerialVehicles.UAVs.Haron;

import AIF.AIFUtil;
import AIF.Missions.MissionTypeException;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class EitanTest {

    public static AIFUtil aifUtil;

    @BeforeClass
    public static void setUpBeforeClass(){
        aifUtil = new AIFUtil();
    }

    @Test
    public void testEitanIntelligenceMission() {
        try{
            aifUtil.getAerialVehiclesHashMap().get("Eitan").setMission(aifUtil.getAllMissions().get("intelligence"));
            assertTrue(true);
        }catch (MissionTypeException missionTypeException){//if exception is thrown it is mean that mission cannot be deploy with the curen aerial vehicle.
            fail();
        }
    }

    @Test
    public void testEitanAttackMission() {
        try{
            aifUtil.getAerialVehiclesHashMap().get("Eitan").setMission(aifUtil.getAllMissions().get("attack"));
            assertTrue(true);
        }catch (MissionTypeException missionTypeException){//if exception is thrown it is mean that mission cannot be deploy with the curen aerial vehicle.
            fail();
        }
    }

    @Test(expected = MissionTypeException.class)//Executable Mission -> BDAMission
    public void testEitanUnexecutableMission() throws MissionTypeException{
        aifUtil.getAerialVehiclesHashMap().get("Eitan").setMission(aifUtil.getAllMissions().get("bda"));
    }
}
package AIF.AerialVehicles.UAVs.Haron;

import AIF.AIFUtil;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class HaronTest {
    public static AIFUtil aifUtil;
    private final int hours;
    private final int expected;

    public HaronTest(int hours, int expected) {
        this.hours = hours;
        this.expected = expected;
    }
    @BeforeClass
    public static void setUpBeforeClass() {
        aifUtil = new AIFUtil();
    }

    @Parameterized.Parameters(name = "{index} input:{0}, output:{1}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                //Limit values
                {-1, 0}, {0, 0}, {1, 1}, {149, 149}, {150, 0}, {151, 0},
                //Middle values
                {-6, 0}, {23, 23}, {199, 0}
        });
    }

    @Test
    public void haronCheckTest()
    {
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Eitan"),hours);
        assertEquals("failure - hoursOfFlightSinceLastRepair should be " + expected,
                expected, aifUtil.getAerialVehiclesHashMap().get("Eitan").getHoursOfFlightSinceLastRepair());
    }
}
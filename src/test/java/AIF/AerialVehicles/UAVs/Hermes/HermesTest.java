package AIF.AerialVehicles.UAVs.Hermes;

import AIF.AIFUtil;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class HermesTest {

    public static AIFUtil aifUtil;
    private final int hours;
    private final int expected;

    public HermesTest(int hours, int expected) {
        this.hours = hours;
        this.expected = expected;
    }
    @BeforeClass
    public static void setUpBeforeClass() {
        aifUtil = new AIFUtil();
    }

    @Parameterized.Parameters(name = "{index} input:{0}, output:{1}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                //Limit values
                {-1, 0}, {0, 0}, {1, 1}, {99, 99}, {100, 0}, {101, 0},
                //Middle values
                {-10, 0}, {23, 23}, {109, 0}
        });
    }

    @Test
    public void hermesCheckTestS()
    {
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Kohav"),hours);
        assertEquals("failure - hoursOfFlightSinceLastRepair should be " + expected,
                expected, aifUtil.getAerialVehiclesHashMap().get("Kohav").getHoursOfFlightSinceLastRepair());
    }
}
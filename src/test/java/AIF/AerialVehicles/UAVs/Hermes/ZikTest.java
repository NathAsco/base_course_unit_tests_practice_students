package AIF.AerialVehicles.UAVs.Hermes;

import AIF.AIFUtil;
import AIF.Missions.MissionTypeException;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class ZikTest {

    public static AIFUtil aifUtil;

    @BeforeClass
    public static void setUpBeforeClass(){
        aifUtil = new AIFUtil();
    }

    @Test
    public void testZikBDAMission() {
        try{
            aifUtil.getAerialVehiclesHashMap().get("Zik").setMission(aifUtil.getAllMissions().get("bda"));
            assertTrue(true);
        }catch (MissionTypeException missionTypeException){//if exception is thrown it is mean that mission cannot be deploy with the curen aerial vehicle.
            fail();
        }
    }

    @Test
    public void testZikIntelligenceMission() {
        try{
            aifUtil.getAerialVehiclesHashMap().get("Zik").setMission(aifUtil.getAllMissions().get("intelligence"));
            assertTrue(true);
        }catch (MissionTypeException missionTypeException){//if exception is thrown it is mean that mission cannot be deploy with the curen aerial vehicle.
            fail();
        }
    }

    @Test(expected = MissionTypeException.class)//Executable Mission -> AttackMission
    public void testZikUnexecutableMission() throws MissionTypeException{
        aifUtil.getAerialVehiclesHashMap().get("Zik").setMission(aifUtil.getAllMissions().get("attack"));
    }

}